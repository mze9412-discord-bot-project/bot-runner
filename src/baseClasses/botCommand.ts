// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-runner>.
//
// Bot-Runner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { BaseCommandInteraction } from "discord.js";
import { SlashCommandBuilder, SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder } from "@discordjs/builders"
import { injectable } from "inversify";

@injectable()
export abstract class BotCommand {
    constructor(private _name: string, private _description: string, private _permissions: string) {
    }

    get name(): string { return this._name; }
    get description(): string { return this._description; }
    get permissions(): string { return this._permissions; }

    abstract buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder;
    abstract isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean>;
    abstract execute(interaction: BaseCommandInteraction): Promise<void>;

}