// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-runner>.
//
// Bot-Runner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { BaseCommandInteraction, Client, CommandInteractionOptionResolver } from "discord.js";
import { SlashCommandBuilder, SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder } from "@discordjs/builders";
import { BotCommand } from "./botCommand";
import { injectable, unmanaged } from "inversify";
import { BotLogger } from "..";
import { Logger } from "tslog";

@injectable()
export abstract class BotModule {
    private _commands: BotCommand[] = [];
    private _client!: Client;

    private _moduleName = '';
    private _moduleDescription = '';

    private _logger!: Logger;
    public get logger(): Logger { return this._logger; }

    constructor(@unmanaged() moduleName: string, @unmanaged() moduleDescription: string, @unmanaged() botLogger: BotLogger) {        
        this._moduleName = moduleName;
        this._moduleDescription = moduleDescription;
        this._logger = botLogger.getChildLogger(moduleName);
    }

    get name(): string { return this._moduleName; }
    get description(): string { return this._moduleDescription; }
    get client(): Client { return this._client; }

    async initialize(client: Client): Promise<void> {
        this._client = client;

        await this.initializeCore();
    }

    abstract initializeCore(): Promise<void>;

    abstract start(): Promise<void>;
    abstract stop(): Promise<void>;

    protected addSubCommand(command: BotCommand): BotModule {
        this._commands.push(command);
        return this;
    }    

    async buildCommands(configCommandBuilder: SlashCommandBuilder): Promise<SlashCommandBuilder[]> {
        const commands: SlashCommandBuilder[] = [];

        // create main module command
        const commandBuilder = new SlashCommandBuilder();
        commandBuilder.setName(this._moduleName);
        commandBuilder.setDescription(this._moduleDescription);

        // build commands
        for(const cmd of this._commands) {
            const result = cmd.buildCommand();
            if (result instanceof SlashCommandSubcommandBuilder) {
                commandBuilder.addSubcommand(result);
            } else if(result instanceof SlashCommandSubcommandGroupBuilder) {
                commandBuilder.addSubcommandGroup(result);
            } else if (result instanceof SlashCommandBuilder) {
                // if a command is top level, directly add it
                commands.push(result);
            }
        }

        // add main module command to list
        commands.push(commandBuilder);

        // add own config logic to config command
        await this.buildConfigCommand(configCommandBuilder);

        return commands;
    }

    abstract buildConfigCommand(configCommandBuilder: SlashCommandBuilder): Promise<void>;
    abstract handleConfigCommand(interaction: BaseCommandInteraction): Promise<void>

    public async handleCommand(interaction: BaseCommandInteraction): Promise<boolean> {
        if (interaction.isCommand()) {
            // check if we find a regular command for this first
            for (const command of this._commands) {
                if (await command.isMatching(interaction, this._moduleName)) {
                    await command.execute(interaction);
                    return true;
                }
            }

            // special case: this is the config command
            const opts = <CommandInteractionOptionResolver>interaction.options;
            if (interaction.commandName === 'configure' && opts.getSubcommand() === this.name) {
                this._logger.info(`Handling configure command (${interaction.commandName}). Responsible module: ${opts.getSubcommand()}. This module: ${this.name}`)
                await this.handleConfigCommand(interaction);
                return true;
            }
        }
        
        return false;
    }
}