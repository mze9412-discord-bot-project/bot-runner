// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-runner>.
//
// Bot-Runner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { inject, injectable } from "inversify";
import { BotModule } from "../baseClasses/botModule";
import { DummyCommand } from "../commands/dummyCommand";
import { SlashCommandBuilder } from "@discordjs/builders";
import { BaseCommandInteraction, CommandInteractionOption, CommandInteractionOptionResolver } from "discord.js";
import { TYPES_BOTRUNNER } from "../types";
import { BotLogger } from "..";
import { Logger } from "tslog";

@injectable()
export class DefaultModule extends BotModule {
    constructor(@inject(TYPES_BOTRUNNER.BotLogger) private _botLogger: BotLogger) {
        super('defaultmodule', 'This is the default module', _botLogger);
    }

    async initializeCore(): Promise<void> {
        this.addSubCommand(new DummyCommand());
    }

    async buildConfigCommand(configCommandBuilder: SlashCommandBuilder): Promise<void> {
        configCommandBuilder.addSubcommand(scmd =>
            scmd.setName(this.name)
            .setDescription('Module specific configuration')
            .addStringOption(opt =>
                opt.setName('greeting')
                .setDescription('How do you want to be greeted?')
                .setRequired(true)
            )
        );
    }

    async handleConfigCommand(interaction: BaseCommandInteraction): Promise<void> {
        const opts = <CommandInteractionOptionResolver>interaction.options;
        const subCommand = <CommandInteractionOption[]>opts.data;
        const options = <CommandInteractionOption[]>subCommand[0].options;

        for(const o of options) {
            this.logger.debug(`Configured option: ${o.name}. Value: ${o.value}`);
        }

        await interaction.reply(`Configuration for module ${this.name} finished!`);
    }

    async start(): Promise<void> {
        // do nothing
    }
    async stop(): Promise<void> {
        // do nothing
    }

}