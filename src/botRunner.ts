// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-runner>.
//
// Bot-Runner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { BaseCommandInteraction, Client, Guild, Intents, Interaction, RateLimitData } from "discord.js";
import { REST } from "@discordjs/rest"
import { SlashCommandBuilder } from "@discordjs/builders"
import { inject, injectable, multiInject } from "inversify";
import { BotRunnerConfig } from "./botRunnerConfig";
import { BotModule } from "./baseClasses/botModule";
import { ConfigService } from "./services/configService";
import { DiscordChannelService } from "./services/discordChannelService";
import { DiscordMessageService } from "./services/discordMessageService";
import { DiscordUserService } from "./services/discordUserService";
import { TYPES_BOTRUNNER } from "./types";
import { RESTPostAPIApplicationCommandsJSONBody, Routes } from "discord-api-types/v9";
import { BotLogger } from ".";
import { Logger } from "tslog";

/**
 * Main Bot Runner interaction class. This is the root for running my discord bots.
 * It recieves all bot modules and initializes them and their commands and connects to Discord with the specified data.
 */
@injectable()
export class BotRunner {
    private _client!: Client;
    private _rest!: REST;
    private _logger!: Logger

    /**
     * Constructor
     * @param _modules array of bot modules to use for bot initialization
     */
    constructor(
        @multiInject(TYPES_BOTRUNNER.BotModule) private _modules: BotModule[],
        @inject(TYPES_BOTRUNNER.DiscordChannelService) private _discordChannelService: DiscordChannelService,
        @inject(TYPES_BOTRUNNER.DiscordMessageService) private _discordMessageService: DiscordMessageService,
        @inject(TYPES_BOTRUNNER.DiscordUserService) private _discordUserService: DiscordUserService,
        @inject(TYPES_BOTRUNNER.ConfigService) private _configService: ConfigService,
        @inject(TYPES_BOTRUNNER.BotLogger) private _botLogger: BotLogger
        ) {
        this._logger = this._botLogger.getChildLogger("BotRunner");
        this.createClient().then(() => this._logger.info("Initialized all modules."));
    }

    /**
     * Client getter
     */
    public get client(): Client {
        return this._client;
    }

    /**
     * REST getter
     */
    public get rest(): REST {
        return this._rest;
    }

    private get config(): BotRunnerConfig {
        return <BotRunnerConfig>this._configService.configs.get('botRunner');
    }

    /**
     * Helper method to create and initialize a new client
     * @param logInfos if true bot header and infos are logged
     */
    private async createClient(logInfos = true): Promise<void> {
        this._client = new Client({
            intents: [
                Intents.FLAGS.GUILDS,
                Intents.FLAGS.GUILD_BANS,
                Intents.FLAGS.GUILD_MEMBERS,
                Intents.FLAGS.GUILD_MESSAGES,
                Intents.FLAGS.GUILD_PRESENCES,
                Intents.FLAGS.GUILD_WEBHOOKS
            ]
        });
        this._rest = new REST({ version: '9' }).setToken(this.config.loginToken);

        // make sure all modules register their commands
        for(const module of this._modules) {
            this._logger.info(`Initializing ${module.name} ...`);
            await module.initialize(this.client);
            this._logger.info(`Initializing ${module.name} ... finished!`);
        }

        // make sure services used by this bot instance use correct client
        this._discordChannelService.client = this.client;
        this._discordMessageService.client = this.client;
        this._discordUserService.client = this.client;

        // register event handlers
        this.client.on('ready', async () => this.handleOnReady());
        this.client.on('warn', async warningMsg => this.handleOnWarning(warningMsg));
        this.client.on('guildUnavailable', async guild => this.handleOnGuildUnavailable(guild));
        this.client.on('guildCreate', async guild => this.handleOnGuildCreate(guild));
        this.client.on('guildDelete', async guild => this.handleOnGuildDelete(guild));
        this.client.on('rateLimit', async info => this.handleOnRateLimit(info));
        this.client.on('interactionCreate', async interaction => this.handleInteraction(interaction));

        if(logInfos) this.logBotHeaderInfo();
    }

    /**
     * Helper method to log bot header and all basic bot config and command lists for debugging.
     */
    private logBotHeaderInfo(): void {
        this._logger.info(
              "                      _____    ___  __   _____  ______         _   ______                                 \n"
            + "                     |  _  |  /   |/  | / __  \ | ___ \       | |  | ___ \                                \n"
            + " _ __ ___   ____ ___ | |_| | / /| |`| | `' / /' | |_/ /  ___  | |_ | |_/ /_   _  _ __   _ __    ___  _ __ \n"
            + "| '_ ` _ \ |_  // _ \\____ |/ /_| | | |   / /   | ___ \ / _ \ | __||    /| | | || '_ \ | '_ \  / _ \| '__|\n"
            + "| | | | | | / /|  __/.___/ /\___  |_| |_./ /___ | |_/ /| (_) || |_ | |\ \| |_| || | | || | | ||  __/| |   \n"
            + "|_| |_| |_|/___|\___|\____/     |_/\___/\_____/ \____/  \___/  \__|\_| \_|\__,_||_| |_||_| |_| \___||_|   \n"
            + "                                                                                                          \n"
            + "                                                                                                          \n"
        );
    }

    /**
     * Starts the bot by logging into discord
     */
    public async start(): Promise<void> {
        if (this.config.isTestingMode) {
            this._logger.warn('### BOT IS IN TESTING MODE. DO NOT USE THIS MODE IN PRODUCTION.');
        }

        this._logger.info('Logging bot into Discord.');
        try {
            const loginResult = await this.client.login(this.config.loginToken);
            this._logger.info('Login done. Login token: ' + loginResult);

            this._logger.info('Starting all modules.');
            for (const module of this._modules) {
                await module.start();
            }
            this._logger.info('Finished starting all modules.');
        } catch(ex) {
            this._logger.error(ex);
        }
    }

    /**
     * Stops the bot and creates a new client
     */
    public async stop(): Promise<void> {
        this._logger.info('Stopping all modules.');
        for (const module of this._modules) {
            await module.start();
        }
        this._logger.info('Finished stopping all modules.');

        // destroy / logout client and create new one for reuse
        this._logger.info('Logging bot out of Discord.');
        this.client.destroy();
        this.createClient(false);
    }

    /**
     * Handle interaction events for Slash Commands
     * @param interaction 
     */
    private async handleInteraction(interaction: Interaction): Promise<void> {
        if (interaction.isCommand()) {
            const commandInteraction = <BaseCommandInteraction>interaction;
            for(const module of this._modules) {
                if(await module.handleCommand(commandInteraction)) {
                    break;
                }
            }
        }
    }

    /**
     * Event handler for ready event
     */
    private async handleOnReady(): Promise<void> {
        this._logger.info(`I am ready! Logged in as ${this.client.user?.tag}!`);
        this._logger.info(`Bot has started, with ${this.client.users.cache.size} users, in ${this.client.channels.cache.size} channels of ${this.client.guilds.cache.size} guilds.`);

        // collect all commands from all modules
        const commands: RESTPostAPIApplicationCommandsJSONBody[] = [];

        // create config command
        const configCommandBuilder = new SlashCommandBuilder()
            .setName('configure')
            .setDescription('Configure bot usage');

        for(const module of this._modules) {
            const moduleCommands = await module.buildCommands(configCommandBuilder);
            for(const cmd of moduleCommands) {
                commands.push(cmd.toJSON());
            }
        }

        // add config command
        commands.push(configCommandBuilder.toJSON());

        // register commands for all guilds
        for(const guild of this._client.guilds.cache.values()) {
            await this._rest.put(Routes.applicationGuildCommands(this.config.clientId, guild.id), { body: commands });
        }
        this._logger.info(`Registered a total of ${commands.length} commands.`)

        // list guilds
        for (const guild of this.client.guilds.cache) {
            const owner = await this._discordUserService.findUser(guild[1].ownerId);

            this._logger.info('List of guilds:');
            if (owner == undefined) {
                this._logger.info(`- ${guild[1].name} (ID ${guild[1].id}), Owner ID ${guild[1].ownerId}`);
            } else {
                this._logger.info(`- ${guild[1].name} (ID ${guild[1].id}), Owner ${owner.username} (ID ${owner.id})`);
            }
            this._logger.info('End list of guilds.');
        }

        this.client.user?.setActivity('mze9412 - Software on Demand!');

        this._logger.info('Informing owner about ready state.');
        await this._discordMessageService.sendPrivateMessageToUserId(this.config.ownerDiscordId, 'I am now online and running!');
    }

    /**
     * Event handler for guildCreate event
     * @param guild newly joined guild
     */
    private async handleOnGuildDelete(guild: Guild): Promise<void> {
        this._logger.info(`Left guild ${guild.name} (ID: ${guild.id}).`);
    }

    /**
     * Event handler for guildDelete event
     * @param guild left guild
     */
    private async handleOnGuildCreate(guild: Guild): Promise<void> {
        this._logger.info(`Joined guild ${guild.name} (ID: ${guild.id}).`);
    }

    /**
     * Event handler for guildUnavailable event
     * @param guild unavailable guild
     */
    private async handleOnGuildUnavailable(guild: Guild): Promise<void> {
        this._logger.info(`Guild ${guild.name} (ID: ${guild.id}) unavailable.`);
    }

    /**
     * Event handler for warnings from discord
     * @param warningMsg the warning merssage
     */
    private async handleOnWarning(warningMsg: string): Promise<void> {
        this._logger.warn('Discord Warning: ' + warningMsg);

        // inform bot owner
        this._discordMessageService.sendPrivateMessageToUserId(this.config.ownerDiscordId, warningMsg);
    }
    
    /**
     * Event handler for rate limit from discord
     * @param info rate limit data
     */
    private async handleOnRateLimit(info: RateLimitData): Promise<void> {
        const msg = `Rate Limit Hit.\nTimout: ${info.timeout}\nLimit: ${info.limit}\nMethod: ${info.method}\nPath: ${info.path}\nRoute: ${info.route}`;
        this._logger.warn(msg);

        // inform bot owner
        this._discordMessageService.sendPrivateMessageToUserId(this.config.ownerDiscordId, msg);
    }
}
