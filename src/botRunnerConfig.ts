// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-runner>.
//
// Bot-Runner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

export class BotRunnerConfig {
    /**
     * Constructor
     * @param _ownerDiscordId 
     * @param _loginToken 
     * @param _clientId 
     * @param _isTestingMode 
     */
    constructor(private _ownerDiscordId: string, private _loginToken: string, private _clientId: string, private _isTestingMode: boolean) {
    }

    get ownerDiscordId(): string { return this._ownerDiscordId; }
    get loginToken(): string { return this._loginToken; }
    get clientId(): string { return this._clientId; }
    get isTestingMode(): boolean { return this._isTestingMode; }
}