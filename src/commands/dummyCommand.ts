import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder } from "@discordjs/builders";
import { BaseCommandInteraction, CommandInteractionOption, CommandInteractionOptionResolver } from "discord.js";
import { BotCommand } from "../baseClasses/botCommand";

export class DummyCommand extends BotCommand {
    constructor() {
        super('dummy', 'A dummy command!', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder {
        const subCmdBuilder = new SlashCommandSubcommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(opt => 
                opt.setName('username')
                    .setDescription('Your name!')
                    .setRequired(true)
            );

        return subCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {
        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }
        
        const opts = <CommandInteractionOptionResolver>interaction.options;
        if (opts.getSubcommand() === this.name) {
            const subCommand = <CommandInteractionOption[]>opts.data;
            const options = subCommand[0].options;
            if (options != undefined && options?.length > 0 && options[0].name === 'username') {
                await interaction.reply(`Hi ${options[0].value}!`);
            }
        }
    }
}