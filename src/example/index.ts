
import "reflect-metadata";
import { BotLogger, BotRunner, BotRunnerConfig, ConfigService, TYPES_BOTRUNNER } from "..";
import { myContainer } from "./inversify.config";

const log = myContainer.get<BotLogger>(TYPES_BOTRUNNER.BotLogger).logger;

// register global callbacks
process.once("uncaughtException", err => {
    log.error(err);
});

// register global callbacks
process.once("uncaughtException", err => {
    log.error(err);
});

// log working dir
log.info(`Working Directory: ${process.cwd()}`);

// get config service and add config for botrunner
const configService = myContainer.get<ConfigService>(TYPES_BOTRUNNER.ConfigService);
configService.addConfig('botRunner', new BotRunnerConfig('owner discord id', 'login token', 'client id', true));                // replace with your own config

// get bot
const bot = myContainer.get<BotRunner>(TYPES_BOTRUNNER.BotRunner);
bot.start();

// register to SIGINT and SIGTERM for graceful shutdown
process.once('SIGINT', () => {
    log.info('SIGINT: Stopping');
    bot.stop();
    log.info('SIGINT: DONE');
    process.exit(0);
});
process.once('SIGTERM', () => {
    log.info('SIGTERM: Stopping');
    bot.stop();
    log.info('SIGTERM: DONE');
    process.exit(0);
});