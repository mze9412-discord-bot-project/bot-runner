// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-runner>.
//
// Bot-Runner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { Container } from "inversify";
import { BotLogger, BotModule, BotRunner, ConfigService, DefaultModule, DiscordChannelService, DiscordMessageService, DiscordUserService, TYPES_BOTRUNNER } from "..";

const myContainer = new Container();
myContainer.bind<ConfigService>(TYPES_BOTRUNNER.ConfigService).to(ConfigService).inSingletonScope();
myContainer.bind<BotRunner>(TYPES_BOTRUNNER.BotRunner).to(BotRunner).inSingletonScope();
myContainer.bind<BotModule>(TYPES_BOTRUNNER.BotModule).to(DefaultModule).inSingletonScope();

myContainer.bind<DiscordChannelService>(TYPES_BOTRUNNER.DiscordChannelService).to(DiscordChannelService);
myContainer.bind<DiscordMessageService>(TYPES_BOTRUNNER.DiscordMessageService).to(DiscordMessageService);
myContainer.bind<DiscordUserService>(TYPES_BOTRUNNER.DiscordUserService).to(DiscordUserService);

myContainer.bind<BotLogger>(TYPES_BOTRUNNER.BotLogger).to(BotLogger).inSingletonScope();

export { myContainer };