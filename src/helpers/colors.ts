// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-runner>.
//
// Bot-Runner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { HexColorString } from "discord.js";

export abstract class Colors {
    public static green: HexColorString = '#78B159';
    public static red: HexColorString = '#DD2E44';
    public static blue: HexColorString = '#5DADEC';
    public static yellow: HexColorString = '#FDCB58';
}