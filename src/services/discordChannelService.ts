// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-runner>.
//
// Bot-Runner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { CategoryChannel, Client, TextChannel, VoiceChannel } from "discord.js";
import { injectable } from "inversify";

/**
 * Service for interacting with discord channels
 */
@injectable()
export class DiscordChannelService {
    private _client!: Client;
        
    public get client(): Client {
        return this._client;
    }
    
    public set client(value: Client) {
        this._client = value;
    }

    public async getVoiceChannel(guildId: string, channelId: string): Promise<VoiceChannel|undefined> {
        let guild = await this.client.guilds.fetch(guildId);
        guild = await guild.fetch();
        return <VoiceChannel>guild.channels.cache.find(x => x.id === channelId);
    }

    public async getVoiceChannelByName(guildId: string, channelName: string): Promise<VoiceChannel|undefined> {
        let guild = await this.client.guilds.fetch(guildId);
        guild = await guild.fetch();
        return <VoiceChannel>guild.channels.cache.find(x => x.name === channelName && x.type === 'GUILD_VOICE');
    }

    public async getTextChannel(guildId: string, channelId: string): Promise<TextChannel|undefined> {
        let guild = await this.client.guilds.fetch(guildId);
        guild = await guild.fetch();
        return <TextChannel>guild.channels.cache.find(x => x.id === channelId);
    }

    public async getTextChannelByName(guildId: string, channelName: string): Promise<TextChannel|undefined> {
        let guild = await this.client.guilds.fetch(guildId);
        guild = await guild.fetch();
        return <TextChannel>guild.channels.cache.find(x => x.name === channelName && x.isText());
    }

    public async getCategory(guildId: string, channelId: string): Promise<CategoryChannel|undefined> {
        let guild = await this.client.guilds.fetch(guildId);
        guild = await guild.fetch();
        return <CategoryChannel>guild.channels.cache.find(x => x.id === channelId);
    }
    
    public async getCategoryByName(guildId: string, channelName: string): Promise<CategoryChannel|undefined> {
        let guild = await this.client.guilds.fetch(guildId);
        guild = await guild.fetch();
        return <CategoryChannel>guild.channels.cache.find(x => x.name === channelName && x.type === 'GUILD_CATEGORY');
    }

    public async createTextChannel(guildId: string, channelName: string, category: CategoryChannel): Promise<TextChannel> {
        let guild = await this.client.guilds.fetch(guildId);
        guild = await guild.fetch();

        return await guild.channels.create(channelName, { parent: category, type: 'GUILD_TEXT' });
    }

    public async createVoiceChannel(guildId: string, channelName: string, category: CategoryChannel): Promise<VoiceChannel> {
        let guild = await this.client.guilds.fetch(guildId);
        guild = await guild.fetch();

        return await guild.channels.create(channelName, { parent: category, type: 'GUILD_VOICE' });
    }

    public async createCategory(guildId: string, channelName: string): Promise<CategoryChannel> {
        let guild = await this.client.guilds.fetch(guildId);
        guild = await guild.fetch();

        return await guild.channels.create(channelName, { type: 'GUILD_CATEGORY' });
    }
}