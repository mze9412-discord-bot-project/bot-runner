// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Runner <https://gitlab.com/mze9412-discord-bot-project/bot-runner>.
//
// Bot-Runner is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Runner is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Runner. If not, see <http://www.gnu.org/licenses/>.

import { Client, Message, MessageEmbed, TextChannel, User, Util } from "discord.js";
import { inject, injectable } from "inversify";
import { Logger } from "tslog";
import { BotLogger } from "../helpers/botLogger";
import { TYPES_BOTRUNNER } from "../types";

/**
 * Service for interacting with discord messages
 */
@injectable()
export class DiscordMessageService {
    private _client!: Client;
    private _logger!: Logger;    

    constructor(@inject(TYPES_BOTRUNNER.BotLogger) private _botLogger: BotLogger) {
        this._logger = _botLogger.getChildLogger('DiscordMessageService');
    }

    public get client(): Client {
        return this._client;
    }

    public set client(value: Client) {
        this._client = value;
    }
    
    public async sendPrivateMessage(user: User|undefined, msg: string): Promise<void> {
        if (user !== undefined) {
            const messages = Util.splitMessage(msg);
            for (const message of messages) {
                await user.send(message);
            }
            this._logger.debug(`Sent private message to ${user.username}.`);
        } else {
            this._logger.warn(`Could not find user to deliver private message.`)
        }
    }

    public async sendPrivateMessageToUserId(userId: string, message: string): Promise<void> {
        const user = await this.client.users.fetch(userId);
        await this.sendPrivateMessage(user, message);
    }

    public async getMessage(messageId: string, channel: TextChannel): Promise<Message|undefined> {
        await channel.messages.fetch();
        return <Message>channel.messages.cache.find(x => x.id === messageId);
    }

    public async sendTextMessage(channel: TextChannel, msg: string): Promise<string> {        
        let msgId = '0';

        // send message
        const messages = Util.splitMessage(msg);
        for (const message of messages) {
            msgId = await (await channel.send(message)).id;
        }
        return msgId;
    }

    public async sendEmbedMessage(channel: TextChannel, msg: MessageEmbed): Promise<string> {        
        // send message
        const message = await channel.send({ embeds: [ msg ] });
        return message.id;
    }
}